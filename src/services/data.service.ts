import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  baseUrl : any = 'http://localhost:3000/';
  constructor(private http:HttpClient) { }

  getRequest(url:any){
   return this.http.get(this.baseUrl+url);
  }

  postRequest(url:any ,body:any){
    return this.http.post(this.baseUrl+url,body);
  }
}
