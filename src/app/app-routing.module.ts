import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path:'dynamic',
    loadChildren:()=> import('./module/dynamic/dynamic.module').then(m=>m.DynamicModule)
  },
  {
    path:'home',
    loadChildren:()=>import('./module/home/home.module').then(m=>m.HomeModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
