import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DynamicRoutingModule } from './dynamic-routing.module';
import { DynamicComponent } from './dynamic/dynamic.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    DynamicComponent
  ],
  imports: [
    CommonModule,
    DynamicRoutingModule,
    FormsModule
  ]
})
export class DynamicModule { }
