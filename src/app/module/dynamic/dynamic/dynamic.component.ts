import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/services/data.service';

@Component({
  selector: 'app-dynamic',
  templateUrl: './dynamic.component.html',
  styleUrls: ['./dynamic.component.css']
})
export class DynamicComponent implements OnInit {

  type: any = null;
  validation: boolean = false;
  myiput: any;
  isValidationSelected: boolean = false;
  name: any = "";
  dynamicField: any;
  inputList : any = [];
  constructor(private dataservice:DataService) { }

  ngOnInit(): void {
    this.getInputList();
  }
  
  getInputList(){
    this.dataservice.getRequest('posts').subscribe(
      (res:any)=>{
        this.inputList = res;
      }
    );
  }
  saveName(dynamicForm:any){
    let obj ={
      "value":dynamicForm.value.dynamicField
    }
    this.dataservice.postRequest('data',obj).subscribe(
      (res)=>{
        console.log(res);
        
      }
    )
  }

  checkType(validation: any) {
    if (validation=='true') {
      this.isValidationSelected = true;
    }else {
      this.isValidationSelected = false;
    }   
    console.log(this.isValidationSelected);
    
  }



}
